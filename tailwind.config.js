const colors = require('tailwindcss/colors')

module.exports = {
  purge: {
    content: [
      './src/**/*.{js,jsx,ts,tsx}',
      './src/**/**/*.{js,jsx,ts,tsx}',
      './public/index.html'
    ],
    options: {
      safelist: {
        standard: [/group-hover/,/bg-red/,/bg-gray/,/bg-purple/,/bg-amber/,/bg-teal/,/bg-pink/,/bg-blue/,/text-red/,/text-gray/,/text-purple/,/text-amber/,/text-teal/,/text-pink/,/text-blue/,/border-red/,/border-gray/,/border-purple/,/border-amber/,/border-teal/,/border-pink/,/border-blue/],
        deep: [/foo$/],
        greedy: [/foo$/]
      }
    },
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      gray: colors.gray,
      green: colors.lime,
      yellow: colors.yellow,
      white: 'white',
      black: 'black',
      blue: colors.blue,
      red: colors.red,
      purple: colors.fuchsia,
      pink: colors.pink,
      teal: colors.teal,
      amber: colors.amber
    },
    fontFamily: {
      mono: ['ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace'],
      sans: ['ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"']
    },
    extend: {
      width: {
         'reading': '612px'
       },
       animation: {
         fade: 'fadeOut 5s ease-in-out',
       },
       keyframes: theme => ({
         fadeOut: {
           '0%': { backgroundColor: theme('colors.blue.300') },
           '100%': { backgroundColor: theme('colors.transparent') },
         },
       })
    }
  },
  variants: {
    extend: {
      opacity: ['disabled'],
    }
  },
  plugins: [
    require('@tailwindcss/typography')
  ],
}
