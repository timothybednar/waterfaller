const FormatPercent = new Intl.NumberFormat('en-US', {
  style: 'percent',
  useGrouping: 'false',
  minimumFractionDigits: 0
});

export default FormatPercent;
