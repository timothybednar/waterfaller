function toggleClassName(element,className='hidden',e){
  console.log(e);
  const header = e.target.parentElement;
  header.classList.toggle('expanded');
  e.target.classList.toggle('clicked');
  document.getElementById(element).classList.toggle(className);
}

export default toggleClassName;
