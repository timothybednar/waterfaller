function handleWorkSpaceView (workspace){
  if (workspace==='screenshot') {
    document.getElementById("waterfall").classList.add("hidden");
    document.getElementById("screenshot").classList.remove("hidden");
    document.getElementById("tab-screenshot").classList.remove("opacity-50");
    document.getElementById("tab-waterfall").classList.add("opacity-50");
  }
  if (workspace==='waterfall') {
    document.getElementById("waterfall").classList.remove("hidden");
    document.getElementById("screenshot").classList.add("hidden");
    document.getElementById("tab-screenshot").classList.add("opacity-50");
    document.getElementById("tab-waterfall").classList.remove("opacity-50");
  }
  return null;
}

export default handleWorkSpaceView;
