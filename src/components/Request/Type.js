import React from 'react';

class Type extends React.Component {

  checkType(type,src){
    if (type!=="Image") {
      return (
        <div>{type}</div>
      )
    } else {
      return (
        <div>
          <p><a href={src} target="_blank" rel="noreferrer">foo<img src={process.env.DEMO !== ? src : "https://via.placeholder.com/120x213"} alt="Open in new tab" className="border border-black"/></a></p>
        </div>
      )
    }
  }

  render(){
    return (
      this.checkType(this.props.type,this.props.url)
    )
  }

}

export default Type;
