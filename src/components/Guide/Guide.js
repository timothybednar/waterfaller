import React from 'react';
import ReactMarkdown from "react-markdown";

const ATag = props => {
  return (
    <a
      className="hover:underline"
      href={props.href}
      target="_blank"
      rel="noreferrer"
    >
      {props.children}
    </a>
  );
};

class Guide extends React.Component {

  clickFilter(fix){
    document.getElementById(`filter-${fix}`).click();
    let filtersEl = document.getElementById(`filters`);
    filtersEl.scrollIntoView();
  }

  downloadFile(string){
    var FileSaver = require('file-saver');
    var blob = new Blob([string], {type: "text/plain;charset=utf-8"});
    FileSaver.saveAs(blob, "waterfallermarkdown.txt");
  }

  render(){
    return (
      <div className="p-4 prose">
        <h2>This is your playbook</h2>
        <p>The theme clearly states your goals. Waterfaller evaluates each Core Web Vital by combining the analysis of this run and the available Chrome User Experience (CrUX) data from Google. This assessment drives how epics and stories are written and priorized.</p>
        <button
          className="animate-pulse w-full rounded my-2 bg-gray-600 my-1 hover:bg-white border-2 border-gray-600 hover:text-gray-600 text-white col-span-2 inline-block mr-2 uppercase px-1 py-0.5 outline-none focus:outline-none mr-3"
          onClick={() => this.downloadFile(this.props.markdown)}
        >
          Download Playbook TXT
        </button>
        <div className="text-black border-l-4 pl-4 border-red-500">
            <ReactMarkdown
              source={`${this.props.theme}`}
              renderers={{
                link: ATag
              }}
            />
            <button
              className="animate-pulse w-full rounded my-2 bg-gray-600 my-1 hover:bg-white border-2 border-gray-600 hover:text-gray-600 text-white col-span-2 inline-block mr-2 uppercase px-1 py-0.5 outline-none focus:outline-none mr-3"
              onClick={() => this.clickFilter('theme')}
            >
              View Theme
            </button>
        </div>
        <h3>(1) Review your theme</h3>
        <p>Each metric is rated to see if they <strong>passed, regressed, validated, or failed</strong> expectations.</p>
        <ol>
          <li><em>Regressions</em> indicate that this run's results are worse than the CrUX data indicates. This can indicate that a recent release negatively impacted your performance.</li>
          <li><em>Validations</em> mean the opposite because this run's results are better than the CrUX data available.</li>
          <li>Passing means that your page passed expectations on this run and with the CrUX data avaible.</li>
          <li>Failing means that your page did not pass requirements on this run, nor using the CrUX data available.</li>
        </ol>
        <p>These evaluations should determine a level of urgency with <strong>failed and regressed</strong> metrics requiring attention. The goal is to pass each Core Web Vital metric.</p>
        <h3>(2) Prioritize epics</h3>
        <p>Epics correlate to the Core Web Vitals defined by Google and reported in its Search Console. Each epic represents a body of work that is then broken down into corresponding tasks or user-stories.</p>
        <div className="text-black border-l-4 pl-4 border-red-500">
          <ReactMarkdown
            source={`${this.props.epics}`}
            renderers={{
              link: ATag
            }}
          />
          <button
            className="animate-pulse w-full rounded my-2 bg-gray-600 my-1 hover:bg-white border-2 border-gray-600 hover:text-gray-600 text-white col-span-2 inline-block mr-2 uppercase px-1 py-0.5 outline-none focus:outline-none mr-3"
            onClick={() => this.clickFilter('epic')}
          >
            View Epics
          </button>
        </div>
        <p>So for the epic to be considered done, all its tasks need to be completed. Epics state the goal that drives each task.</p>
        <h3>(3) Audit your tasks by epic</h3>
        <p>Each task rolls up to an epic. Tasks are full-blown user stories that provide a <strong>problem statement, task, solutions, and testing criteria</strong>. Our algorithm formulates starts with a PageSpeed Insights report, adds over a dozen custom audits, and then formulates a one-of-a-kind guide to improving Core Web Vitals.</p>
        <h3>(4) Build your backlog</h3>
        <p>Each task or user story rolls up to an epic. These are precise and ready for developers. They discussed, prioritized against other work, and ultimately used for sprint planning.</p>
        <h3>Understand your "ask"</h3>
        <p>This playbook is designed to help you make a <strong>quality ask of developers</strong>. It assumes that you will spend some time with this document so you can understand the problems and solutions. This is the only way to get good results. Waterfaller was not designed as a shortcut that replaces logic, experience, or research.</p>
      </div>
    )
  }
}

export default Guide;
