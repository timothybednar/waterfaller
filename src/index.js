import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Test from './components/App/App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <Test />,
  document.getElementById('test')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals(console.log);
